﻿using System;
using System.Runtime.InteropServices;

namespace MvcApplication.Services
{
    public class CrtUtils
    {
        //打开串口
        [DllImport("CRT_C100.dll")]
        public static extern UInt16 C100_OpenPort(int nPort);

        //关闭串口
        [DllImport("CRT_C100.dll")]
        public static extern int C100_ClosePort();

        //复位读卡机
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Reset(byte[] rxData, ref Int16 rxDataLen);

        //轮询
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Poll(byte[] rxData, ref Int16 rxDataLen);

        //获取纸币控制器识别的当前纸币面值
        [DllImport("CRT_C100.dll")]
        public static extern int C100_GetBillTable(byte[] rxData, ref Int16 rxDataLen);

        //控制器发送识别器返回机器号，软件版本号等信息
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Identification(byte[] rxData, ref Int16 rxDataLen);

        //设置纸币控制器识别的纸币面值   //true 为命令方式，false为自动方式
        [DllImport("CRT_C100.dll")]
        public static extern int C100_EnableBillType(byte cBillType, bool autoStack, byte[] rxData, ref Int16 rxDataLen);

        //打开识别器识别的具体面值及自动存钞
        [DllImport("CRT_C100.dll")]
        public static extern int C100_EnableBillStack(byte[] rxData, ref Int16 rxDataLen);

        //打开识别器识别的具体面值、并命令存钞/存钞/持币
        [DllImport("CRT_C100.dll")]
        public static extern int C100_EnableBill(byte[] rxData, ref Int16 rxDataLen);

        //关闭识别器识别
        [DllImport("CRT_C100.dll")]
        public static extern int C100_DisableBill(byte[] rxData, ref Int16 rxDataLen);

        //退钞
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Return(byte[] rxData, ref Int16 rxDataLen);

        //存钞
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Stack(byte[] rxData, ref Int16 rxDataLen);

        //持币
        [DllImport("CRT_C100.dll")]
        public static extern int C100_Hold(byte[] rxData, ref Int16 rxDataLen);


    }
}