﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using MvcApplication.Services;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public JsonResult Index()
        {
            var person = new
            {
                Name = "liu",
                Age = 32,
                Sex = "男"
            };
            return Json(person, JsonRequestBehavior.AllowGet);
        }


        //打开串口
        public JsonResult OpenPort(int nPort = 0)
        {
            if (nPort <= 0)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }

            return Json(CrtUtils.C100_OpenPort(nPort), JsonRequestBehavior.AllowGet);

        }

        //关闭串口
        public JsonResult ClosePort()
        {
            return Json(CrtUtils.C100_ClosePort(), JsonRequestBehavior.AllowGet);

        }

        //复位读卡机
        public JsonResult Reset(byte[] rxData, Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Reset(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);
        }

        //轮询
        public JsonResult Poll(byte[] rxData, Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Poll(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);
        }


        //获取纸币控制器识别的当前纸币面值
        public JsonResult GetBillTable(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_GetBillTable(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);
        }

        //控制器发送识别器返回机器号，软件版本号等信息
        public JsonResult Identification(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Identification(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);
        }

        //设置纸币控制器识别的纸币面值   //true 为命令方式，false为自动方式
        public JsonResult EnableBillType(byte cBillType, bool autoStack, byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_EnableBillType(cBillType, autoStack, rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);
        }

        //打开识别器识别的具体面值及自动存钞
        public JsonResult EnableBillStack(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_EnableBillStack(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

        //打开识别器识别的具体面值、并命令存钞/存钞/持币
        public JsonResult EnableBill(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_EnableBill(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

        //关闭识别器识别
        public JsonResult DisableBill(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_DisableBill(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

        //退钞
        public JsonResult Return(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Return(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

        //存钞
        public JsonResult Stack(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Stack(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

        /**
         * 持币
         */
        public JsonResult Hold(byte[] rxData, ref Int16 rxDataLen)
        {
            return Json(CrtUtils.C100_Hold(rxData, ref rxDataLen), JsonRequestBehavior.AllowGet);

        }

    }
}
